import { Auth0Plugin } from "./vueauth.js";

const empty = {
    customer: {
        name: "",
        phone: "",
        email: ""
    },
    choice: {
        choice: "",
        size: "",
        extras: []
    },
    lieferzeit: new Date(),
}

Vue.component("orders", {
    props: {
        value: {
            type: Array
        }
    },
    template: `
    <div>
    <table id="orders">
       <tr><th>Kunde</th><th>Pizza</th><th>Lieferzeit</th></tr>
       <tr v-for="order in value">
       <td>{{ order.customer.name }}</td>
       <td>{{ order.choice.choice }}</td>
       <td>{{ new Date(order.lieferzeit).toLocaleString() }}</td>
       <td><button v-on:click="showDetails" v-bind:data-id="order._id"><img src="edit-24px.svg" v-bind:data-id="order._id"/></button></td>
       <td><button v-on:click="deleteOrder" v-bind:data-id="order._id"><img src="delete-24px.svg" v-bind:data-id="order._id"/></button></td>
       </tr>
    </table>
    <button v-on:click="newOrder"><img src="create-24px.svg"/></button>
    </div>
    `,
    methods: {
        showDetails: function (event) {
            this.$parent.getOrder(event.target.dataset.id)
                .then(order => {
                    console.log("order set to %o %o", order, this.$parent.order);
                    order.lieferzeit = new Date(order.lieferzeit);
                    console.log("parent: %o", this.$parent);
                    this.$parent.order = Vue.observable(order);
                    window.form.hidden = false;
                });
        },
        newOrder: function (event) {
            this.$parent.order = Vue.observable(empty);
            window.form.hidden = false;
        },

        deleteOrder: async function (event) {
            let id = event.target.dataset.id;
            console.log('deleting order %s', id);
            let headers = await this.$parent.getAuthHeader();
            return fetch(`api/order/${id}`, {
                method: 'DELETE',
                headers: headers
            })
                .then(res => {
                    console.log('server response: %s', res.status);
                    for (let i = 0; i < this.value.length; i++) {
                        if (this.value[i]._id === id) {
                            console.log("deleting order #%d", i);
                            this.value.splice(i, 1);
                        }
                    }
                })
                .catch(error => console.log("error: %o", error));
        },
    }
});

Vue.component("customer", {
    props: {
        value: {
            name: "",
            phone: "",
            email: "",
            location: ""
        }
    },
    template: `
    <fieldset>
      <legend> Kundendaten </legend>
      <p>
        <label for="customer">Name:</label>
        <input id="customer" v-model="value.name" v-on:input="handleInput" required="true" />
      </p>
      <p>
        <label for="phone">Telefon:</label>
        <input id="phone" type="tel" v-model="value.phone" v-on:input="handleInput">
      </p>
      <p>
        <label for="email">Email:</label>
        <input id="email" type="email" v-model="value.email" v-on:input="handleInput">
      </p>
      <p>
        <label for="location">Standort:</label>
        <select id="location" v-model="value.location" v-on:input="handleInput">
          <option value="hagen" accesskey="h">Hagen</option>
          <option value="iserlohn" selected="true">Iserlohn</option>
          <option value="meschede">Meschede</option>
          <option value="soest">Soest</option>
        </select>
      </p>
    </fieldset>`,
    methods: {
        handleInput: function (event) {
            // console.log("input: %o", event);
            // console.log("value: %o", this.value);
            this.$emit('input', this.value);
        }
    }
});


Vue.component("choice", {
    props: {
        value: {
            choice: "",
            size: "medium",
            extras: []
        }
    },
    template: `
  <fieldset>
    <legend>Auswahl</legend>
    <label for="choice">Gewünschte Pizza:</label>
    <input list="pizza-flavors" id="choice" v-model="value.choice" v-on:input="handleInput" required="true" />
  
    <fieldset>
      <legend> Größe </legend>
      <div><label> <input type="radio" value="small" v-model="value.size" v-on:input="handleInput"> Klein </label></div>
      <div><label> <input type="radio" value="medium" v-model="value.size" v-on:input="handleInput"> Mittel </label></div>
      <div><label> <input type="radio" value="large" v-model="value.size" v-on:input="handleInput"> Groß </label></div>
    </fieldset>
  
    <fieldset class="block">
      <legend> Extras </legend>
      <label> <input type="checkbox" value="mozzarella" v-model="value.extras" v-on:input="handleInput"> Mozzarella</label>
      <label> <input type="checkbox" value="zwiebeln" v-model="value.extras" v-on:input="handleInput"> Zwiebeln</label>
      <label> <input type="checkbox" value="champignons" v-model="value.extras" v-on:input="handleInput"> Champignons</label>
      <label> <input type="checkbox" value="salami" v-model="value.extras" v-on:input="handleInput"> Salami</label>
    </fieldset>
    <datalist id="pizza-flavors">
      <option value="Margherita"/>
      <option value="Funghi"/>
      <option value="Salami"/>
      <option value="Prosciutto"/>
      <option value="Tonno"/>
    </datalist>
  </fieldset>`,
    methods: {
        handleInput: function (event) {
            // console.log("input: %o", event);
            // console.log("extras: %o", this.value.extras);
            this.$emit('input', this.value);
        }
    }
});

Vue.component("lieferzeit", {
    props: {
        value: {
            type: Date,
            default: new Date()
        }
    },
    data: function () {
        console.log("lieferzeit data: %o", this.value);
        let iso = this.value.toISOString();
        let time = this.value.toLocaleTimeString().substring(0, 4);
        let date = iso.substring(0, 10);
        console.log("date: %s, time: %s", date, time);
        return {
            'time': time,
            'date': date,
            'error': null
        }
    },
    template: `
  <fieldset>
    <legend>Gewünschter Lieferzeitpunkt</legend>
    <input type="time" name="delivery_time" v-model="time" v-on:input="handleInput" />
    <input type="date" name="delivery_date" v-model="date" v-on:input="handleInput" />
    <p v-if="error" class="error">
      {{ error }}
    </p>
  </fieldset>`,
    methods: {
        handleInput: function (event) {
            console.log("date: %s, time: %s", this.date, this.time);
            let value = Date.parse(this.date + " " + this.time);
            console.log("date_time: " + value);
            this.error = null;
            if (value < Date.now()) {
                this.error = "Entschuldigen Sie bitte, aufgrund eines Problems mit unserer Zeitmachine liefern wir aktuell nicht in die Vergangenheit!";
            }
            this.value = new Date(value);
            this.$emit('input', this.value);
        }
    },
    watch: {
        value: {
            handler: function (val, oldVal) {
                console.log("lieferzeit set: %o", val);
                let iso = this.value.toISOString();
                this.time = this.value.toLocaleTimeString().substring(0, 5);
                this.date = iso.substring(0, 10);
            },
            deep: true
        }
    }
});



window.onload = function () {
    this.console.log('Initializing auth0')

    Vue.use(Auth0Plugin, {
        domain: 'cgawron.eu.auth0.com',
        clientId: 'wcNAM2F0yGSVGxTPGa8pX2Ww8Jbl090K',
        cacheLocation: 'localstorage',
        audience: 'https://jupiter.fh-swf.de/pizza/api',
        redirectUri: 'https://jupiter.fh-swf.de/pizza/#callback',
        onRedirectCallback: appState => {
            router.push(
                appState && appState.targetUrl
                    ? appState.targetUrl
                    : window.location.pathname
            );
        }
    });

    console.log("initializing vue");

    new Vue({
        el: "#app",
        watch: {
            /* 
             choice: {
                 handler: function (val, oldVal) {
                     console.log("choice: %s", JSON.stringify(val));
                 },
                 deep: true
             },
             instructions: {
                 handler: function (val, oldVal) {
                     console.log("instructions: %s", JSON.stringify(val));
                 }
             }
             */
        },
        methods: {
            login() {
                console.log('login')
                this.$auth.loginWithPopup()
                    .then(() => { this.getOrders() })
                    .then(orders => this.orders = orders)
            },
            // Log the user out
            logout() {
                this.$auth.logout({
                    returnTo: 'https://jupiter.fh-swf.de/pizza'
                });
            },
            getAuthHeader: async function () {
                const token = await this.$auth.getTokenSilently();
                const myHeaders = new Headers();
                myHeaders.append('Authorization', `Bearer ${token}`);
                return myHeaders
            },
            getOrders: function () {
                return this.getAuthHeader()
                    .then(headers => {
                        return fetch("api/order", { headers: headers })
                    })
                    .then(response => {
                        if (response.status == 401) {
                            console.log('got 401, redirecting to /login');
                            //this.login();
                        }
                        else return response.json();
                    })
                    .then(orders => {
                        console.log('server response: %o', orders);
                        return orders;
                    })
                    .catch(error => console.log("error: %o", error));
            },
            getOrder: async function (id) {
                console.log("getOrder(%s)", id);
                return fetch(`api/order/${id}`, { headers: await this.getAuthHeader() })
                    .then(response => response.json())
                    .then(order => {
                        console.log('server response: %o', order);
                        return order;
                    })
                    .catch(error => console.log("error: %o", error));
            },
            cancelOrder: function (event) {
                event.preventDefault();
                window.form.hidden = true;
            },
            saveOrder: async function (event) {
                event.preventDefault();
                window.form.hidden = true;

                if (this.order._id) {
                    let id = this.order._id;
                    console.log("saving order %s", id);

                    let headers = await this.getAuthHeader();
                    headers.append('Content-Type', 'application/json');
                    fetch(`api/order/${id}`, {
                        method: 'PUT',
                        headers: headers,
                        body: JSON.stringify(this.order, function (key, value) {
                            if (key[0] === '_') {
                                return undefined;
                            } else {
                                return value;
                            }
                        })
                    })
                        .then(response => response.json())
                        .then(response => {
                            console.log('server response: %o', response);
                            // vue.js does not support maps as values, so we need to search here ... 
                            for (let i = 0; i < this.orders.length; i++) {
                                if (this.orders[i]._id === id) {
                                    console.log("replacing order %d with %o", i, this.order);
                                    Vue.set(this.orders, i, this.order);
                                    break;
                                }
                            }
                        })
                        .catch(error => console.log("error: %o", error));
                } else {
                    this.createOrder(event);
                }
            },
            createOrder: async function (event) {
                console.log("createOrder: %o, order: %o", event, this.order);
                event.preventDefault();
                let headers = await this.getAuthHeader();
                headers.append('Content-Type', 'application/json');
                fetch("api/order", {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(this.order)
                })
                    .then(response => response.json())
                    .then(response => {
                        console.log('server response: %o', response);
                        let n = this.orders.length;
                        Vue.set(this.orders, n, this.order);
                    })
                    .catch(error => console.log("error: %o", error));
            },
            loadOrders: function (event) {
                console.log('loadOrders')
                this.getOrders().then(orders => {
                    this.orders = orders
                })
            },
            loadUserInfo: async function (event) {
                console.log('loading user info')
                let headers = await this.getAuthHeader();
                this.userInfo = await (await fetch('https://cgawron.eu.auth0.com/userinfo', { headers: headers })).json();
                console.log("user info: %o", this.userInfo);
            },
        },
        created: function () {
            this.$auth.$on('authLoaded', () => {
                console.log('auth loaded');
                this.loadUserInfo();
                this.loadOrders();
            });
            this.$auth.$on('loggedIn', () => {
                console.log('logged in');
                this.loadUserInfo();
                this.loadOrders();
            })
        },
        data: {
            orders: [],
            order: empty,
            userInfo: {}
        },
    });
}