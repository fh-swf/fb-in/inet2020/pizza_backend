module.exports = {
    root: true,
    env: {
        "browser": true,
        "es6": true,
        "node": true,
    },
    plugins: [
    ],
    extends: [
        'eslint:recommended',

    ],
    rules: {
        "arrow-parens": ["error", "as-needed"],
        "require-await": "off",
    },
};