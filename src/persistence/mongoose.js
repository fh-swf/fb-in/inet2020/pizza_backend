/**
 * Persistance layer using Mongoose and MongoDB
 *
 * @copyright 2020 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 */

const mongoose = require('mongoose');

const url = process.env.MONGO_URL;
const dbName = 'pizza';
const choiceSchema = new mongoose.Schema({
    choice: String,
    size: {
        type: String,
        enum: ["small", "medium", "large"]
    },
    extras: [{
        type: String,
        enum: ["salami", "mozzarella", "zwiebeln", "champignons"]
    }]
}, { _id: false });
const customerSchema = new mongoose.Schema({
    name: { type: String },
    phone: String,
    email: String,
    location: String
}, { _id: false });
const orderSchema = new mongoose.Schema({
    lieferzeit: { type: Date },
    instructions: { type: String },
    customer: customerSchema,
    choice: choiceSchema,
});

class DB {
    connect() {
        console.log("connecting to %s", url + '/' + dbName);
        orderSchema.set('toJSON', { virtuals: true });
        DB.Order = mongoose.model('orders', orderSchema);

        return mongoose.connect(url + '/' + dbName, { useNewUrlParser: true, useUnifiedTopology: true })
            .catch(err => {
                console.log("error connecting to mongodb uri=%s: %o", url, err);
                return Promise.reject({ err: err, message: "mongo connection failed" });
            })
            .then(db => {
                DB.db = db;
                // collection = db.collection('orders');
            })

    }

    queryAll() {
        return DB.Order.find({});
    }
}

module.exports = new DB();
