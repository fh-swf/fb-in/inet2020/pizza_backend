/**
 * Persistance layer using a SQL database
 *
 * @copyright 2020 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 */

const { Sequelize, DataTypes } = require('sequelize');

const dbName = "pizza";
const username = process.env.USERNAME;
const password = process.env.PASSWORD;

const ORDER = {
    "customer": { "name": "Knecht Rubrecht", "phone": "", "email": "rubrecht@weihnachtsmann.np", "location": "iserlohn" },
    "choice": { "choice": "Bescherung", "size": "large", "extras": ["salami", "mozzarella", "zwiebeln", "champignons"] },
    "lieferzeit": "2019-10-25T10:30:00.000Z",
    "instructions": "mit Allem und mehr"
}

let db = null;
let collection = null;
class DB {
    connect() {
        const sequelize = new Sequelize(dbName, username, password, {
            host: 'localhost',
            dialect: 'mariadb'
        });


        return sequelize.authenticate()
            .catch(err => {
                console.log("error connecting to db %s: %o", dbName, err);
                return Promise.reject({ err: err, message: "db connection failed" });
            })
            .then(() => {
                console.log("db connected");
                const Customer = sequelize.define('customer', {
                    // Model attributes are defined here
                    name: {
                        type: DataTypes.STRING,
                        allowNull: false
                    },
                    email: {
                        type: DataTypes.STRING
                    },
                    phone: {
                        type: DataTypes.STRING
                    },
                    location: {
                        type: DataTypes.STRING
                    }
                }, {});

                const Order = sequelize.define('order', {
                    lieferzeit: {
                        type: DataTypes.DATE,
                        allowNull: false
                    },
                    instructions: {
                        type: DataTypes.STRING
                    }
                }, {});

                const Choice = sequelize.define('choice', {
                    choice: {
                        type: DataTypes.STRING,
                        allowNull: false
                    },
                    size: {
                        type: DataTypes.ENUM,
                        values: ['small', 'medium', 'large']
                    },
                }, {});


                const Extra = sequelize.define('extra', {
                    extra: {
                        type: DataTypes.ENUM,
                        values: ['mozzarella', 'zwiebeln', 'champignons', 'salami'],
                        primaryKey: true
                    },
                }, {
                    timestamps: false
                });

                const Extro = DataTypes.ENUM(['mozzarella', 'zwiebeln', 'champignons', 'salami']);

                Customer.belongsTo(Order);
                Order.hasOne(Customer);
                Order.hasOne(Choice);
                Choice.hasMany(Extra);
                //Extra.belongsToMany(Choice, { through: 'OrderExtras' });

                DB.Customer = Customer;
                DB.Order = Order;
                DB.Choice = Choice;
                DB.Extra = Extra;

                return sequelize.sync({ force: true })
            })
            .then(async () => {
                let customer = await DB.Customer.create(ORDER.customer);
                let order = await DB.Order.create({
                    lieferzeit: ORDER.lieferzeit,
                    instructions: ORDER.instructions
                });
                let choice = await DB.Choice.create({
                    choice: ORDER.choice.choice,
                    size: ORDER.choice.size
                });

                order.setCustomer(customer);
                order.setChoice(choice);
                for (let extra of ORDER.choice.extras) {
                    if (extra) {
                        let _extra = await DB.Extra.create({ extra: extra });
                        await choice.addExtra(_extra);
                        //await _extra.addOrder(order);
                        await _extra.save();
                    }
                }
                await order.save();
                await customer.save();
            })
    }

    queryAll() {
        let result = DB.Order.findAll({
            include: [
                DB.Customer,
                {
                    model: DB.Choice,
                    include: {
                        model: DB.Extra,
                        attributes: ['id', 'extra']
                    }
                }
            ]
        });
        console.log("queryAll: %o", result);
        return result;
    }

    queryById(id) {
        return collection.findOne({ _id: new ObjectId(id) });
    }

    update(id, order) {
        return collection.replaceOne({ _id: new ObjectId(id) }, order);
    }

    delete(id) {
        return collection.deleteOne({ _id: new ObjectId(id) });
    }

    insert(order) {
        return collection.insertOne(order);
    }
}

module.exports = new DB();