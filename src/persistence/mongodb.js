/**
 * Persistance layer using MongoDB
 *
 * @copyright 2019 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 */

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = process.env.MONGO_URL;
const dbName = 'pizza';

let db = null;
let collection = null;
class DB {
    connect() {
        return MongoClient.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
            .catch(err => {
                console.log("error connecting to mongodb uri=%s: %o", url, err);
                return Promise.reject({ err: err, message: "mongo connection failed" });
            })
            .then(function (client) {
                db = client.db(dbName);
                collection = db.collection('orders');
            })
    }

    queryAll() {
        return collection.find().toArray();
    }

    queryById(id) {
        return collection.findOne({ _id: new ObjectId(id) });
    }

    update(id, order) {
        return collection.replaceOne({ _id: new ObjectId(id) }, order);
    }

    delete(id) {
        return collection.deleteOne({ _id: new ObjectId(id) });
    }

    insert(order) {
        return collection.insertOne(order);
    }
}

module.exports = new DB();