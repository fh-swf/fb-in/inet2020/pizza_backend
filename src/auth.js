const passport = require('passport');
const AnonymousStrategy = require('passport-anonymous').Strategy;
const session = require('express-session');
const { custom, Issuer, Strategy } = require('openid-client');
const auth0_config = require('../auth0.json')
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;


custom.setHttpOptionsDefaults({
    followRedirect: true,
    timeout: 10000,
});

class JWT_Authenticator {



    constructor() {
        this.keyPEM = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8Thg156woJzdg7HP/rKt
aIOcIntoqoP3BW0tQnb8sMV2e3hLSys2BW80o6KnqZjd1UNZenKW/u9j1Yq9oHxE
34/6u5KmBO5AAXn1M+QkLa2Pp8TvxL+8IdaiNpzS5ZSmHkrFmN/gijRPOKsYWbEt
PfsGCO5sMjj1VbbyHGVwT7TKJ7kVHbHFH1jAVB6nl7ibLUM0169UjWWTnI8zaWZN
pZNToTGpIMGclDHowuO/eO81epfAj2OE7zKR98zjYiLRsEkJRB6fSSsMu8Dt+Ph0
xsCB/klQviYJ77ztAlRkbm7su2m5loOqWXhNw+IFOG+LPrkZncX3dsyxtluuvuvn
DwIDAQAB
-----END PUBLIC KEY-----`

        this.extractor = function () {
            let auth_scheme = "Bearer";
            var auth_scheme_lower = auth_scheme.toLowerCase();
            return function (request) {

                var token = null;
                var re = /(\S+)\s+(\S+)/;

                function parseAuthHeader(hdrValue) {
                    if (typeof hdrValue !== 'string') {
                        return null;
                    }
                    var matches = hdrValue.match(re);
                    return matches && { scheme: matches[1], value: matches[2] };
                }

                if (request.headers['authorization']) {
                    var auth_params = parseAuthHeader(request.headers['authorization']);
                    if (auth_params && auth_scheme_lower === auth_params.scheme.toLowerCase()) {
                        token = auth_params.value;
                    }
                }
                console.log("raw token: %s", token);
                return token;
            };
        };

        this.opts = {
            jwtFromRequest: this.extractor(),
            secretOrKey: this.keyPEM,
            issuer: 'https://cgawron.eu.auth0.com/',
            audience: 'https://jupiter.fh-swf.de/pizza/api'
        }
    }

    init(app) {
        this.strategy = new JwtStrategy(this.opts, function (jwt_payload, done) {
            console.log("options: %o, payload: %o", this.opts, jwt_payload)
            let user = jwt_payload;
            console.log('user: %o', user);
            return done(null, user)
        })
        passport.use(this.strategy);
        app.use(passport.initialize());

        return Promise.resolve(this.strategy);
    }
}


module.exports = new JWT_Authenticator();
