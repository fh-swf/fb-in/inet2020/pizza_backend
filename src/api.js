/**
 * API implementation
 *
 * @copyright 2019 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 */


let db = require('./persistence/mongodb.js');

/** Alternative Implementierungen mit Mongoose und Sequelize */
//let db = require('./persistence/mongoose.js');
//let db = require('./persistence/sql.js');

module.exports = {
    /**
     * Get all orders
     */
    getOrders: (req, res) => {
        db.queryAll()
            .then(result => {
                res.send(result);
            })
            .catch(err => {
                console.log("error: %o", err);
                res.status(500).send(err)
            });
    },

    getOrder: (req, res) => {
        // Lese Order aus der Datenbank ...
        let id = req.params.id;
        db.queryById(id)
            .then(result => {
                console.log("queryById: %s => %o", id, result);
                if (!result) {
                    res.status(404).send({});
                } else {
                    res.send(result);
                }
            })
            .catch(err => {
                console.log("error: %o", err);
                res.status(500).send({ error: err, status: 500 })
            });
    },

    /** 
     * Create a new order
     */
    createOrder: (req, res) => {
        console.log("createOrder: %o", req.body);
        db.insert(req.body)
            .then(result => {
                console.log("createOrder: db returned %o", result);
                res.send(result);
            })
            .catch(err => res.status(500).send(err));

    },

    saveOrder: (req, res) => {
        let id = req.params.id;
        console.log("saveOrder: %s %o", id, req.body);
        db.update(id, req.body)
            .then(result => {
                console.log("saveOrder: db returned %o", result);
                res.send(result);
            })
            .catch(err => res.status(500).send(err));
    },

    deleteOrder: (req, res) => {
        let id = req.params.id;
        console.log("deleteOrder: %s %o", id, req.body);
        db.delete(id, req.body)
            .then(result => {
                console.log("deleteOrder: db returned %o", result);
                res.status(204).send();
            })
            .catch(err => res.status(500).send(err));
    },

    init: () => {
        return db.connect();
    },

    /**
     * @todo Verify body data
     */
    verify: (req, res, next) => {
        console.log("verify body: %s %o", req.get('content-type'), req.body);
        next();
    }
}