/**
 * Beispielapplikation "Zentraler Pizzaservice" aus der Vorlesung Internettechnologien, Sommersemester 2020
 * Diese Datei implementiert den Webserver mithilfe von Express.js
 *
 * @copyright 2020 Christian Gawron <gawron.christian@fh-swf.de>
 * @license MIT
 * 
 */
require('dotenv').config();
const express = require('express');
const passport = require('passport');
const cors = require('cors');
const api = require('./api.js');
//const auth = require('./auth.js');
const app = express();

// define "middlewares"
app.use(express.static('static'));
app.use(cors());
app.use(express.json());  // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(api.verify); // Verify body


/*
auth.init(app)
    .then(() => api.init())
    */
api.init()
    .then(() => {
        // API definition: register callbacks for routes & methods
        app.get('/api/order', /* passport.authenticate('jwt', { session: false }), */ api.getOrders);
        app.post('/api/order', /* passport.authenticate('jwt', { session: false }), */ api.createOrder);

        app.get('/api/order/:id', /* passport.authenticate('jwt', { session: false }), */ api.getOrder);
        app.put('/api/order/:id', /* passport.authenticate('jwt', { session: false }),  */api.saveOrder);
        app.delete('/api/order/:id', /* passport.authenticate('jwt', { session: false }), */ api.deleteOrder);

        app.listen(process.env.PORT);
        console.log("listening on port %s", process.env.PORT);
    })
    .catch((err) => console.error("initialization error: %o", err))

