# Demo: Pizza-Service
In dieser Demo aus der Vorlesung geht es um das Bestellformular des 
"Zentralen Pizza Service der Fachhochschule Südwestfalen" (der leider 
bisher nur in dieser Demo existiert).

Die Anwendung erlaubt das Aufgeben, Ändern und Stornieren einer Bestellung.

## Installation
Diese Implementierung der Anwendung verwendet [Node.js](https://nodejs.org) für den Server, [MongoDB](https://www.mongodb.com) als Datenbank und [Vue.js](https://vuejs.org/) für das Frontend. 

Zur Installation sind folgende Schritte nötig:

### Node.js und npm installieren
Auf der [Download-Seite von Node.js](https://nodejs.org/en/download/) finden Sie Installationspakete für Windows und MacOS sowie Binaries für Linux. Für Debian und Ubuntu gibt es
ebenfalls fertige Installationspakete, die man als `root` wie folgt installiert:
```
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs
``` 

### MongoDB installieren
Die Community-Version des MongoDB Servers kann man [hier](https://www.mongodb.com/download-center/community) herunterladen.

Falls Sie [Docker](https://www.docker.com/products/docker-desktop) installiert haben, kann man MongoDB einfach in einem Container laufen lassen - das passende Image ist auf DockerHub frei verfügbar:
```
docker pull mongo
docker run --name pizzadb mongo
``` 

Die im Quellcode hinterlegte Adresse der Datenbank (`mongodb://172.17.0.2:27017`) sollte im Normalfall zu einer solchen "Container-Installation" passen.

### Repository clonen und Abhängigkeiten installieren
Um die Anwendung lokal laufen zu lassen, clonen Sie das Repository und installieren die Abhängigkeiten:
```
git clone https://gitlab.com/fh-swf/fb-in/inet2020/pizza_backend.git
cd pizza_backend
npm install
```

### Anpassen der Datenbank-Adresse
Je nach Installation muss die Datenabank-URL in der Datei `.env` angepasst werden:
```
MONGO_URL = 'mongodb://localhost:27017'
```

### Anwendung starten
Gestartet wird die Anwendung mit
```
npm start
```

Anschließend sollte die Anwendung über [http://localhost:8080/](http://localhost:8080/) erreichbar sein.

  
## Verwendete URIs
Die Liste der [Bestellungen](http://localhost:8080/) ist unter `http://localhost:8080/index.html` erreichbar. 

Die API-Endpunkte sind wie folgt definiert:
```
app.get('/api/order', api.getOrders);
app.post('/api/order', api.createOrder);
app.get('/api/order/:id', api.getOrder);
app.put('/api/order/:id', api.saveOrder);
app.delete('/api/order/:id', api.deleteOrder);
```

